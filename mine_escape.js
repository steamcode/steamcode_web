var escape_scene = 0;
var escape_scenes = ["mine_cavein.jpg", "mine_monsterfood.jpg", "mine_hero.jpg"];
var escape_text = ["The wall caves in!", "A monster eats the guard!", "A hero slays the monster!"];

function escape_update(){
    if (escape_scene >= escape_scenes.length){
	setup("outside_mine");
	return;
    }
    show_text(escape_text[escape_scene]);
    load_image("images/" + escape_scenes[escape_scene]);
    escape_scene++;
}

function setup_mine_escape(){
    reset_text("");
    $("choices").update("<input type='button' onclick='escape_update()' value='OK'/>");
    escape_update();
}
