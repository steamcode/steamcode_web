steamcode.scenes.outside_mine = function(){
    figures = {
	wall: new steamcode.figure({
	    src: "images/outside_mine/wall.png",
	    position: {x: 730, y: 560},
	    scale: 0.2,
	    call: function(){
		figures.miner3.setTexture(figures.miner3.tex.left);
		figures.miner3.walk_towards(this.standpoint, function(){
		    figures.miner3.say("*smack* I feel I'm doing just what I was built for!");
		    steamcode.npc.skills.free_thought -= 0.1;
		    steamcode.npc.skills.pickaxe += 0.1;

		    if (steamcode.npc.skills.free_thought < 0){
			alert("You have become a normal NPC, Game Over!");
			window.location = "index.html";
		    }
		});
	    }, 
	    standpoint: {x: 750, y: 570, z: 300},
	    depth: 300
	}),

	butterfly: new steamcode.figure({
	    src: "images/outside_mine/butterfly.png",
	    position: {x: 1000, y: 400},
	    scale: 0.3,
	    call: function(){
		figures.miner3.setTexture(figures.miner3.tex.front);
		figures.miner3.walk_towards(this.standpoint, function(){
		    figures.miner3.say("You try to hit the butterfly with your pick-axe...");

		    if (Math.random() < 0.2){
			figures.miner3.say("... and hit it!", 1000);
			alert("Game completed, so far.");
			window.location = "index.html";
		    }else{
			figures.miner3.say("... but miss and instead hit your own toe!", 1000);
		    }
		});
	    }, 
	    standpoint: {x: 1000, y: 700, z: vscale_inv(0.2)},
	    depth: vscale_inv(0.2)
	}),

	hero: new steamcode.figure({
	    src: "images/characters/hjälte1.png",
	    position: {x: 1150, y: 720},
	    scale: 0.2,
	    call: function(){
		figures.miner3.setTexture(figures.miner3.tex.right);
		figures.miner3.walk_towards(this.standpoint, function(){
		    figures.miner3.say("Take me out of this hell-hole!");
		    figures.hero.say("So, you are free now, gz! What is my reward?", 2000);
		    figures.miner3.say("... take me out ... of this hell-hole?", 4000);
		    figures.hero.say("What?? OMG! *dissapears*", 6000);
		    figures.miner3.say("(confused, in your head): that wasn't what I meant to say?", 10000);
		    setTimeout(function(){
			figures.hero.visible = false;
		    }, 8000);
		});
	    }, 
	    standpoint: {x: 1000, y: 700, z: vscale_inv(0.2)},
	    depth: vscale_inv(0.2)
	}),

	miner3: $.extend(
	    new steamcode.figure({
		src: "images/characters/protagonist_front.png",
		position: {x: 1000, y: 700},
		scale: 0.2,
		depth: vscale_inv(0.2)
	    }),
	    {
		tex: {
		    front: PIXI.Texture.fromImage("images/characters/protagonist_front.png"),
		    left: PIXI.Texture.fromImage("images/characters/protagonist_left.png"),
		    right: PIXI.Texture.fromImage("images/characters/protagonist_right.png")
		}
	    }
	),

	tree: $.extend(new steamcode.figure({
	    src: "images/outside_mine/cactus1.png",
	    position: {x: 850, y: 700},
	    scale: 0.3,
	    call: function(){
		figures.miner3.setTexture(figures.miner3.tex.left);
		figures.miner3.walk_towards(this.standpoint, function(){
		    figures.miner3.say("*thunk* This feels strange and new!");
		    steamcode.npc.skills.free_thought += 0.1;
		    steamcode.npc.skills.pickaxe += 0.1;
		    
		    if (Math.random() < steamcode.npc.skills.pickaxe / 10){
			console.log("reload texture");
			figures.tree.hp--;
			if (figures.tree.hp <= 1){
			    figures.tree.say("*chomp*crack*crash*!");
			    figures.tree.set_call(null);
			}
			figures.tree.setTexture(figures.tree.tex[5 - figures.tree.hp]);
		    }
		});
	    }, 
	    standpoint: {x: 900, y: 700, z: vscale_inv(0.2)},
	    depth: vscale_inv(0.2)
	}),
	{
	    tex: [
		PIXI.Texture.fromImage("images/outside_mine/cactus1.png"),
		PIXI.Texture.fromImage("images/outside_mine/cactus2.png"),
		PIXI.Texture.fromImage("images/outside_mine/cactus3.png"),
		PIXI.Texture.fromImage("images/outside_mine/cactus4.png"),
		PIXI.Texture.fromImage("images/outside_mine/cactus5_stamm.png")
	    ],
	    hp: 5
	})
    };

    steamcode.npc.skills.free_thought = 1;

    steamcode.graphics.default_scene(figures, "images/outside_mine/background.jpg", "miner3");
}
