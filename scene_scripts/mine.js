steamcode.scenes.mine = function(){
    var time = 0;

    var talk_miner = function(a){
	figures.miner3.setTexture(figures.miner3.tex.front);
	figures.miner3.walk_towards({x: 440, y: 630, z: 200}, function(){
	    figures.miner3.say("Take me out of this hell-hole!");
	    figures["miner" + a].say("Take me out of this hell-hole!", 1000);
	});
    }

    // ****************************************
    // 	figure data
    // ****************************************

    figures = {
	wall: new steamcode.figure({
	    src: "images/mine/wall.png",
	    position: {x: 135, y: 760},
	    call: function(){
		figures.miner3.setTexture(figures.miner3.tex.left);
		figures.miner3.walk_towards(this.standpoint, function(){
		    if (time > 5){
			escape_monster();
			return;
		    }

		    time += 1;
		    figures.miner3.say("You take a swing at the wall with your pick-axe!");
		});
	    },
	    depth: vscale_inv(0.2),
	    standpoint: {x: 260, y: 750, z: vscale_inv(0.2)}
	}),

	miner1: new steamcode.figure({
	    src: "images/mine/miner1.png", 
	    position: {x: 330, y: 600},
	    scale: 0.1,
	    call: function(){
		figures.miner3.setTexture(figures.miner3.tex.front);
		figures.miner3.walk_towards(figures.miner1.standpoint, function(){
		    figures.miner3.say("Take me out of this hell-hole!");
		    figures.miner1.say("Take me out of this hell-hole!", 1000);
		});
	    },
	    standpoint: {x: 440, y: 600, z: vscale_inv(0.1)},
	    depth: vscale_inv(0.1)
	}),

	miner2: new steamcode.figure({
	    src: "images/mine/miner2.png",
	    position: {x: 300, y: 660},
	    scale: 0.15,
	    call: function(){
		figures.miner3.setTexture(figures.miner3.tex.front);
		figures.miner3.walk_towards(this.standpoint, function(){
		    figures.miner3.say("Take me out of this hell-hole!");
		    figures.miner2.say("Take me out of this hell-hole!", 1000);
		});
	    },
	    standpoint: {x: 430, y: 660, z: vscale_inv(0.15)},
	    depth: vscale_inv(0.15)
	}),

	miner3: $.extend(
	    new steamcode.figure({
		src: "images/characters/protagonist_front.png",
		position: {x: 260, y: 750},
		scale: 0.2,
		call: function(){},
		depth: vscale_inv(0.2),
	    }),
	    {
		tex: {
		    front: PIXI.Texture.fromImage("images/characters/protagonist_front.png"),
		    left: PIXI.Texture.fromImage("images/characters/protagonist_left.png"),
		    right: PIXI.Texture.fromImage("images/characters/protagonist_right.png")
		}
	    }
	),
	
	guard: new steamcode.figure({
	    src: "images/characters/guard1.png",
	    position: {x: 800, y: 750},
	    scale: 0.2,
	    call: function(){
		figures.miner3.setTexture(figures.miner3.tex.right);
		figures.miner3.walk_towards(figures.guard.standpoint, function(){
		    figures.miner3.say("Take me out of this hell-hole!");
		    figures.guard.say("Shut your mouth and keep working, maggot!", 1000);
		});
	    },
	    standpoint: {x: 700, y: 750, z: vscale_inv(0.2)},
	    depth: vscale_inv(0.2)
	}),
	
	monster_head: new steamcode.figure({
	    src: "images/mine/monsterhuvud.png",
	    position: {x: 870, y: 620},
	    scale: 0.3,
	    call: null,
	    depth: vscale_inv(0.2)
	}),

	hero: new steamcode.figure({
	    src: "images/characters/hjälte1.png",
	    position: {x: 800, y: 750},
	    scale: 0.25,
	    call: null,
	    standpoint: {x: 700, y: 750, z: vscale_inv(0.2)},
	    depth: vscale_inv(0.2)
	})
    };

    figures.monster_head.visible = false;
    figures.hero.visible = false;

    // ****************************************
    //     choice handlers
    // ****************************************

    var escape_hero = function(){
	for (var k in figures){
	    figures[k].say("");
	}

	figures.hero.set_call(function(f){
	    figures.miner3.setTexture(figures.miner3.tex.right);
	    figures.miner3.walk_towards(figures.hero.standpoint, function(){
		figures.miner3.say("Take me out of this hell-hole!");
		figures.hero.say("Follow me!", 2000);
		setTimeout(function(){
		    var p = {x: 400, y: 430, z: 400};
		    figures.miner3.walk_towards(p);
		    figures.hero.walk_towards(p, steamcode.scenes.outside_mine);
		}, 3000);
	    });
	});
	figures.miner3.say("... but a HERO appears and slays the monster in a brillian battle!");
	figures.guard.visible = false;
	figures.monster_head.visible = false;
	figures.hero.visible = true;
    }

    var escape_monster = function(){
	// mine escape scene
	figures.monster_head.visible = true;
	for (var k in figures){
	    figures[k].say("!!!", 0, 10000);
	    figures[k].set_call(null);
	}
	figures.miner3.say("The wall caves in and a Monster appears!");
	setTimeout(function(){
	    figures.miner3.say("... the guard gets eaten");
	    figures.guard.visible = false;
	    setTimeout(escape_hero, 2000);
	}, 4000);
    }

    steamcode.graphics.default_scene(figures, "images/mine/background.jpg", "miner3", function(){
	// on load
	figures.guard.say("Miner 3, start swinging that pick-axe!", 500);
    });

};
