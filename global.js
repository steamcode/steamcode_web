var steamcode = {
    scenes: {},

    npc : {
	skills: {
	    pickaxe: 0.1
	}
    }
};

function extend_object(a, b){
    for (var k in b){
	if (typeof(a[k]) == "object" && typeof(b[k]) == "object"){
	    extend_object(a[k], b[k]);
	}else{
	    a[k] = b[k];
	}
    }
}
