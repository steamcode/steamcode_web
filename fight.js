steamcode.fight = function(t1, t2, bkg){

    // make new figures? (need to replace click options)
    // use a seperate stage, so we don't mess up the game stage

    // return {target, ability}
    var select = function(abilities){
	
    };

    // select and apply ability to target for each member of team
    var round = function(team){
	for (k in team){
	    var choice = select(figures[k].preloaded_abilities);
	    figures[k].preloaded_abilities[choice.ability](k, this);
	}
    }

    var count = 0;

    var figures = [];
    var team1 = [];
    var team2 = [];

    // setup figures
    for (var k = 0; k < t1.length; k++){
	figures[count] = t1[k];
	team1[k] = count;
	count++;
    }

    for (var k = 0; k < t2.length; k++){
	figures[count] = t2[k];
	team2[k] = count;
	count++;
    }
    
    // main loop
    while (team1.length > 0 && team2.length > 0){
	round(team1);
	round(team2);
    }
};
