// graphics setup
steamcode.graphics = {
    stage: new PIXI.Stage(0x66FF99),
    renderer: PIXI.autoDetectRecommendedRenderer(1200, 800)
};

steamcode.graphics.renderer.view.className = "renderer_view";

// default scene builder
steamcode.graphics.default_scene = function(figures, bkg, protagonist, callback){

    // set up load indicator
    progressbar = new PIXI.Text("Loading...");
    progressbar.anchor.x = 0.5;
    progressbar.anchor.y = 0.5;
    progressbar.position.x = steamcode.graphics.renderer.width/2;
    progressbar.position.y = steamcode.graphics.renderer.height/2;

    var load_progress = function(){
	requestAnimFrame(load_progress);
	steamcode.graphics.renderer.render(steamcode.graphics.stage);
    };
    steamcode.graphics.stage.removeChildren();
    steamcode.graphics.stage.addChild(progressbar);
    requestAnimFrame(load_progress);

    // animate function
    var animate = function(){
	requestAnimFrame(animate);

	for (var k in figures){
	    for (var j in figures[k].update_me){
		figures[k].update_me[j]();
	    }

	    // if (protagonist != "undefined"){
	    // 	figures[k].filters[0].blur = Math.abs(figures[k].depth - figures[protagonist].depth) / 10;
	    // }
	}

	steamcode.graphics.renderer.render(steamcode.graphics.stage);
    }

    // start loading background
    background = new PIXI.Sprite.fromImage(bkg);
    background.width = steamcode.graphics.renderer.width;
    background.height = steamcode.graphics.renderer.height;

    // start animate when all children loaded
    check_missing = function(){
	var missing = true;
	missing = false;
	for (var k in figures){
	    missing = missing || !figures[k].texture.valid;
	}

	missing = missing || !background.texture.valid;
	
	if (missing){
	    console.log("missing");
	    setTimeout(check_missing, 100);
	}else{
	    console.log("building stage");
	    // build stage
	    steamcode.graphics.stage.removeChildren();
	    steamcode.graphics.stage.addChild(background);
	    for (var i in figures){
    		steamcode.graphics.stage.addChild(figures[i]);
	    }

	    requestAnimFrame(animate);
	    if (callback) callback();
	}
    };

    check_missing();
}

// figure inherits sprite constructor
steamcode.figure = function(data){
    var tex = PIXI.Texture.fromImage(data.src);
    PIXI.Sprite.call(this, tex);

    var scale = typeof(data.scale) == "object" ? data.scale : typeof(data.scale) == "number" ? {x: data.scale, y: data.scale} : {x: 1, y: 1};

    this.position.x = data.position.x;
    this.position.y = data.position.y;
    this.scale.x = scale.x;
    this.scale.y = scale.y;
    this.anchor.x = 0.5;
    this.anchor.y = 1;
    this.interactive = true;
    this.visible = true;
    // this.filters = [new PIXI.BlurFilter()];

    this.depth = data.depth || vscale_inv(scale.x);
    this.standpoint = data.standpoint;
    this.set_call(data.call);
    this.t_start = new Date();
    this.update_me = {};
};

steamcode.figure.prototype = Object.create(PIXI.Sprite.prototype);
steamcode.figure.prototype.constructor = steamcode.figure;

steamcode.figure.prototype.set_call = function(call){
    this.call = call;
    var fig = this;
    this.mouseup = this.touchend = call ? function(data){fig.call();} : null;
}

steamcode.figure.prototype.say = function(text, timeout, time){
    var fig = this;
    setTimeout(function(){
	fig.pratbubbla(text, time);
    }, timeout);
};

// bratbubble :)
steamcode.figure.prototype.pratbubbla = function(text, time){
    var key = "pratbubbla_data";
    time = time || 4000;
    console.log("pb: called: " + text + " - " + time);

    if (key in this){
	// remove graphics objects
	steamcode.graphics.stage.removeChild(this[key].elli);
	steamcode.graphics.stage.removeChild(this[key].gtext);	
	this.position.y = this[key].ybase;
    }
    
    if (!(key in this)){
	// only create update function once
	var fig = this;
	var phase = Math.random() * 2 * Math.PI;

	this.update_me.pratbubbla = function(){
	    var dt = new Date() - fig[key].tstart;
	    var r = dt/fig[key].time;

	    fig[key].gtext.alpha = fig[key].elli.alpha = 1 - Math.pow(r, 5);
	    fig.position.y = fig[key].ybase - (1 - Math.pow(r, 3)) * 40 * Math.abs(Math.sin(dt/100 + phase));

	    if (dt >= fig[key].time){
		console.log("upd: prat: delete");
		fig.position.y = fig[key].ybase;
		steamcode.graphics.stage.removeChild(fig[key].elli);
		steamcode.graphics.stage.removeChild(fig[key].gtext);
		delete fig.update_me.pratbubbla;
		delete fig[key];
	    }
	};
    }

    // build and add graphics objects
    this[key] = {
	time: text.length > 0 ? time : 0,

	gtext: new PIXI.Text(text, {
	    align: "center", 
	    font: "13px Times",
	    wordWrap: true,
	    wordWrapWidth: 150
	}),

	elli: new PIXI.Graphics(),

	tstart: new Date(),

	ybase: this.position.y
    };

    var elli = this[key].elli;
    var gtext = this[key].gtext;

    gtext.anchor.x = 0.5;
    gtext.anchor.y = 0.5;
    gtext.position.x = this.position.x - this.width/2 - gtext.width/2;
    gtext.position.y = this.position.y - this.height - gtext.height/2;

    elli.beginFill(0xFFFFFF);
    elli.lineStyle(2, 0);
    elli.drawEllipse(gtext.position.x, gtext.position.y, gtext.width, gtext.height);
    elli.endFill();

    steamcode.graphics.stage.addChild(elli);
    steamcode.graphics.stage.addChild(gtext);
};

// walk animation
steamcode.figure.prototype.walk_towards = function(_p, callback){
    var p = $.extend(true, {z: _p.z || 0}, _p);
    var speed = 10;
    var tstart = new Date();

    this.say("");

    // load update function
    var fig = this;
    this.update_me.walk_towards = function(){
	var pcurrent = {x: fig.position.x, y: fig.position.y, z: vscale_inv(fig.scale.x)};
	var delta = vdiff(p, pcurrent);
	var dt = new Date() - tstart;

	if (vlength(delta) < 1.5 * speed){
	    delete fig.update_me.walk_towards;
	    fig.rotation = 0;

	    if (callback) callback();
	    return;
	}

	var nv = vscale(delta, 1/vlength(delta));
	var newpos = vadd(pcurrent, vscale(nv, Math.max(speed * vscale_depth(pcurrent.z), 0.1)));
	fig.position.x = newpos.x;
	fig.position.y = newpos.y;
	fig.depth = newpos.z;
	var s = vscale_depth(newpos.z);
	fig.scale.x = fig.scale.y = s;

	fig.rotation = 0.1 * Math.sin(dt / 40);
    }
}
