
<!DOCTYPE html>
<html>  
  <head>
    <meta charset="UTF-8">
    <title>SteamCode: Mine</title>

    <link rel="stylesheet" type="text/css" href="global.css">

    <script src="shared/jquery.min.js"></script>
    <script src="shared/pixi.dev.js"></script>

    <script src="global.js"></script>
    <script src="graphics.js"></script>
    <script src="piximath.js"></script>

    <script src="scene_scripts/mine.js"></script>
    <script src="scene_scripts/outside_mine.js"></script>
  </head>

  <body onload="steamcode.scenes.mine()">
    <script>
      document.body.appendChild(steamcode.graphics.renderer.view);
    </script>
  </body>

</html>
